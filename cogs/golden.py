import discord
from discord.ext import commands
import random
from discord.ext import tasks
from datetime import datetime


"""
Creates Kings

UNFINISHED
"""

class Golden(commands.Cog):
    """Gives a user the golden role"""
    def __init__(self, bot):
        self.bot = bot
        self.guild = None
        self.my_background_task.start()

    async def _golden(self, guild):
        if not self.guild:
            self.guild = self.bot.get_guild(346250609550426123)
            guild = self.guild

        if not discord.utils.get(self.guild.roles, name="Mod"):
            role = await guild.create_role(name="Mod", colour=discord.Colour(0xffb3ee), permissions=discord.Permissions(permissions=2117598961), hoist=True, mentionable=True)
            await role.edit(position=4)

        role = discord.utils.get(self.guild.roles, name="Mod")
        admin = discord.utils.get(self.guild.roles, name="Admin")
        
        oldMods = role.members
        members = self.guild.members

        candidates = []

        for m in members:
            if (not m.bot) and (m not in oldMods) and (m.id != 827151134015225916):
                candidates.append(m)

        amount = len(candidates)//4
        if amount <3:
            amount = 3
        if amount > 5:
            amount = 5

        newMods = random.sample(candidates, k=amount)
        
        try:
            for mod in oldMods:
                await mod.remove_roles(role)
                await mod.remove_roles(admin)
            for mod in newMods:
                await mod.add_roles(role)
                await mod.add_roles(admin)
        except Exception as e:
            print(e)
            return False

        try:
            channel = self.bot.get_channel(827216142337835068)
            message = "**Picked some new mods:**\n"
            for m in newMods:
                message += f"* {m.name}\n"
            await channel.send(message)
        except Exception as e:
            print(e)
            return False

        return True

    
    @tasks.loop(seconds=300) 
    async def my_background_task(self):
        await self._makemods(self.guild)

    @my_background_task.before_loop
    async def before_my_task(self):
        await self.bot.wait_until_ready() # wait until the bot logs in



def setup(bot):
    bot.add_cog(Moderate(bot))
    