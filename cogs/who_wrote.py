import discord
from discord.ext import commands
from config import config
from cogs.utils.create_error import create_error
import json
import keras
import keras.preprocessing.text as kpt
from keras.preprocessing.text import Tokenizer
import numpy as np

from os import path

"""
Log message deletes, edits, member joins/leaves...
"""


class WhoWrote:
    """Log events"""

    def __init__(self, bot):
        self.bot = bot

        dic_filename = path.join(path.dirname(__file__), '../dictionary.json')
        with open(dic_filename, 'r') as dictionary_file:
            self.dictionary = json.load(dictionary_file)

        model_file = path.join(path.dirname(__file__), '../RNN.model')
        self.model = keras.models.load_model("RNN.model")

        self.tokenizer = Tokenizer(num_words=10000)
        self.CATEGORIES = ["northernlion", "egg_dril"]


    @commands.command(pass_context=True, invoke_without_command=False)
    async def author(self, ctx, *args):
        """tells you if northernlion or egg_dril wrote a tweet"""
        await self.bot.send_typing(ctx.message.channel)
        
        def test(inp):
            def convert_text_to_index_array(text):
                words = kpt.text_to_word_sequence(text)
                wordIndices = []
                for word in words:
                    if word in self.dictionary:
                        wordIndices.append(self.dictionary[word])
                return wordIndices

            testArr = convert_text_to_index_array(inp)
            textInput = self.tokenizer.sequences_to_matrix([testArr], mode='binary')
            
            pred = self.model.predict(textInput)
            return f"I think {self.CATEGORIES[np.argmax(pred)]} wrote this tweet ({int(max(pred[0]) * 100)}% sure)"

        try:
            sentence = " ".join(args[0:])
            msg = test(sentence)

            await self.bot.say(msg)

        except Exception as e:
            print(e)
            await self.bot.say(content=None, embed=create_error("checking who it was"))


def setup(bot):
    bot.add_cog(WhoWrote(bot))
