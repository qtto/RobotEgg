import discord
from discord.ext import commands
from cogs.utils.checks import is_owner
from cogs.utils.checks import channels_allowed
from config import config
import random
import asyncio


"""
Add christmas roles
"""


class Christmas(commands.Cog):
    """add christmas roles"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, invoke_without_command=True)
    @channels_allowed(["mod-commands"])
    @is_owner()
    async def christmas(self, ctx):
        members = ctx.message.server.members
        role_one = discord.utils.get(ctx.message.author.server.roles, name="Jolly")
        role_two = discord.utils.get(ctx.message.author.server.roles, name="Merry")

        await self.bot.say("Let's go!")

        count = 0
        
        for member in members:   
            if role_one in member.roles or role_two in member.roles:
                continue
            try:          
                count += 1

                if count%500 == 0:
                    await self.bot.say(f"Added role to {count} members (probably)")
                
                if random.randint(0,100) < 51:
                    await self.bot.add_roles(member, role_one)
                else:
                    await self.bot.add_roles(member, role_two)
            except:
                print(member.name)

            asyncio.sleep(0.1)

        await self.bot.say("We done bois")

    
    async def on_member_join(self, member):
        role_one = discord.utils.get(member.server.roles, name="Jolly")
        role_two = discord.utils.get(member.server.roles, name="Merry")

        if random.randint(0,100) < 51:
            await self.bot.add_roles(member, role_one)
        else:
            await self.bot.add_roles(member, role_two)


# The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case SimpleCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    bot.add_cog(Christmas(bot))
