import discord
from discord.ext import commands
from config import config
from io import BytesIO
import requests


"""
Check DMs
"""

class Modmail(commands.Cog):
    """Relays messages from users to #modmail"""
    def __init__(self, bot):
        self.bot = bot
        self.modmail = None

    
    @commands.command()
    async def winners(self, ctx):
        if not self.modmail:
            self.modmail = self.bot.get_channel(533308539456847882)
        
        if ctx.message.channel == self.modmail and ctx.message.author != self.bot.user:
            if ctx.message.mentions and len(ctx.message.mentions) > 1:
                
                for mention in ctx.message.mentions:
                    try:
                        await mention.send(f"Hey, congratulations! :tada: \nYou won a game in the Eggserver holiday raffle! Here's a spreadsheet with a list of games to choose from. Please take your time and message <@!183630573825163265> (that's '#@Easy#0001 / @Christmas Sparre' in Eggserver) your pick! :partying_face:\n**Link to the game list:**  https://docs.google.com/spreadsheets/d/1iOxT-fmC4j_JNaDJJlsdNMgmfaddGdLir3DQrPYxbIk/edit?usp=drivesdk \n\n(if multiple people choose the same game, the order in which GiveawayBot posted the winners will be used to figure out who gets it)")

                        await self.modmail.send(f"Message sent to <@!{mention.id}>")
                    except:
                        await self.modmail.send(f"Failed to message <@!{mention.id}>")
            return
    

    @commands.Cog.listener()
    async def on_message(self, msg):
        if not self.modmail:
            self.modmail = self.bot.get_channel(533308539456847882)
        
        if msg.guild is None and msg.author != self.bot.user:
            await self.modmail.send(f"**New message from** <@!{msg.author.id}>:\n\n{msg.content}")
            await msg.author.send(embed=await self.create_embed("Message sent!", ""))

            if msg.attachments:
                try:
                    files = []
                    for att in msg.attachments:
                        if att.size >= 5000000:
                            await msg.author.send(embed=await self.create_embed("Sorry...", 
                                            f"I can't send a file you want me to, because the size is too big. Please send the link instead.", 
                                            color=0xd33751))
                            continue

                        files.append(discord.File(fp=BytesIO(requests.get(att.url).content), filename=att.filename))
                    
                    if len(files) > 1:
                        await self.modmail.send(files=files)
                    else:
                        await self.modmail.send(file=files[0])

                except Exception as e:
                    print(e)
                    text = '\n'.join([f'<Attachment> {att.url}' for att in msg.attachments])

                    await self.modmail.send(embed=await self.create_embed("Sorry...", 
                                            f"This message contained attachments, which I failed to relay. Here's some links though: {text}", 
                                            color=0xd33751))
                    
                    await msg.author.send(embed=await self.create_embed("Sorry...", 
                                            "I failed to send some attachments. A mod might ask you to send them again.", 
                                            color=0xd33751))
                                            

        if msg.channel == self.modmail and msg.author != self.bot.user:
            if msg.mentions and len(msg.mentions) == 1:
                try:
                    await msg.mentions[0].send(f"**New message from the mods:**\n\n{msg.content}")

                    await self.modmail.send(embed=await self.create_embed("Message sent:", f"{msg.content}", 
                                            color=0x2acc4d, author=msg.author))
                except:
                    await self.modmail.send(embed=await self.create_embed("Failed to send message:", 
                                            f"{msg.content}", color=0xd33751, author=msg.author))

                await msg.delete()

            if msg.mentions and len(msg.mentions) > 1:
                return
                await self.modmail.send("Too many mentions in message, so I'm not gonna send it.")
            
            
    async def create_embed(self, title, content, color=0xffffff, author=None):
        if author == None:
            author = self.bot.user
        name = author.name 
        
        if not author == self.bot.user:
            name += f" ({author.id})"
        
        emb = discord.Embed(title=title, description=f"\n{content if content else ''}", 
                            color=0xffffff)
        emb.set_author(name=name, icon_url=author.avatar_url)
    
        return emb



def setup(bot):
    bot.add_cog(Modmail(bot))
    