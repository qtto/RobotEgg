import discord
from discord.ext import commands
from config import config
from discord.ext import tasks
from cogs.utils.checks import is_owner
from cogs.utils.checks import channels_allowed


"""
Add/remove a roles to hide channels
"""


class HideChannel(commands.Cog):
    """Toggle voice role"""

    def __init__(self, bot):
        self.bot = bot
        self.guild = None


    def get_guild(self):
        self.guild = self.bot.get_guild(346250609550426123)

    
    def public_channels(self):
        if not self.guild:
            self.get_guild()
        
        everyone_role = self.guild.default_role
        channels = self.guild.text_channels
        public_channels = []

        for channel in channels:
            #dont create role for bot commands
            if channel.id == 346601597335240704:
                continue
            pair = channel.overwrites_for(everyone_role).pair()
            # inverted to check which are hidden, not which are public ("read" is unset, not explicitly allowed)
            if not pair[1].read_messages:
                public_channels.append(channel)
        
        return public_channels


    def is_public(self, channel):
        everyone_role = self.guild.default_role

        #dont create role for bot commands
        if channel.id == 346601597335240704:
            return False
        
        pair = channel.overwrites_for(everyone_role).pair()
        return not pair[1].read_messages


    def create_embed(self, title, text, color, author, avatar_url):
        embed=discord.Embed(color=color)
        embed.set_author(name=author, icon_url=avatar_url)
        embed.add_field(name=title, value=text, inline=True)
        return embed

    
    async def create_hide_role(self, channel):
        print(f"Creating role for {channel.name}.")
        try:
            new_role = await self.guild.create_role(name=f"hide-{channel.name}")
            await channel.set_permissions(new_role, read_messages=False)
        except Exception as e:
            print(f"Error creating role for {channel.name}: {e}")


    async def check_channels(self):
        if not self.guild:
            self.get_guild()

        channels = self.public_channels()
        roles = self.guild.roles

        for channel in channels:
            if any(role.name == f"hide-{channel.name}" for role in roles):
                continue
            else:
                await self.create_hide_role(channel)


    async def check_roles(self):
        if not self.guild:
            self.get_guild()

        channels = self.public_channels()
        roles = self.guild.roles

        for role in roles:
            if role.name.startswith("hide-"):
                if any(channel.name == role.name[5:] for channel in channels):
                    continue
                else:
                    await role.delete(reason="No such public channel")


    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel):
        await self.create_hide_role(channel)


    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        roles = self.guild.roles

        for role in roles:
            if role.name == f"hide-{channel.name}":
                await self.role.delete(reason="Channel removed")
                break


    @commands.Cog.listener()
    async def on_guild_channel_update(self, before, after):
        roles = self.guild.roles

        if before.name != after.name:
            for role in roles:
                if role.name == f"hide-{before.name}":
                    await role.edit(name=f"hide-{after.name}", reason="Channel update")
                    break
        
        else:
            if self.is_public(before) and not self.is_public(after):
                for role in roles:
                    if role.name == f"hide-{before.name}":
                        await self.role.delete(reason="Channel not public anymore.")
                        break
            
            if not self.is_public(before) and self.is_public(after):
                await self.create_hide_role(after)


    @commands.command()
    @is_owner()
    async def hideroles(self, ctx):
        if not self.guild:
            self.get_guild()

        await ctx.send(f"Guild: {self.guild.name} / channels: {len(self.guild.text_channels)} / roles: {len(self.guild.roles)}")
        await self.check_channels()
        await self.check_roles()


    @commands.command()
    @channels_allowed([])
    async def hide(self, ctx):
        if not self.guild:
            self.get_guild()

        channels = ctx.message.clean_content.split(" ")[1:]
        user = ctx.message.author
        roles = self.guild.roles
        msg = ""
        
        for channel in channels:
            try:
                channel = channel.replace("#","")
                role = discord.utils.get(self.guild.roles,name=f"hide-{channel}")
                await user.add_roles(role)
                msg += f"\n - {role.name[5:]}"
            except Exception as e:
                print(e)

        if len(msg) == 0 or len(channels) == 0:
            await ctx.send("No valid channels mentioned.")
            return

        embed = self.create_embed(title="Added to hide list:", 
                                    text=msg, color=0xa1e8b6,
                                    author=user.name, avatar_url=user.avatar_url)
        await ctx.send(embed=embed)


    @commands.command()
    @channels_allowed([])
    async def unhide(self, ctx):
        if not self.guild:
            self.get_guild()

        channels = ctx.message.clean_content.split(" ")[1:]
        user = ctx.message.author
        roles = self.guild.roles
        msg = ""

        if channels and channels[0] == "all":
            for role in user.roles:
                if role.name.startswith("hide-"):
                    await user.remove_roles(role)
                    msg += f"\n - {role.name[5:]}"
        
        else:
            for channel in channels:
                try:
                    channel = channel.replace("#","")
                    role = discord.utils.get(self.guild.roles,name=f"hide-{channel}")
                    await user.remove_roles(role)
                    msg += f"\n - {role.name[5:]}"
                except Exception as e:
                    print(e)

        if len(msg) == 0 or len(channels) == 0:
            await ctx.send("No valid channels mentioned.")
            return
        
        embed = self.create_embed(title="Removed from hide list:", 
                                    text=msg, color=0xe8a1a1,
                                    author=user.name, avatar_url=user.avatar_url)
        await ctx.send(embed=embed)
    
    @commands.command()
    @channels_allowed([])
    async def hidden(self, ctx):
        if not self.guild:
            self.get_guild()

        channels = ctx.message.channel_mentions
        user = ctx.message.author
        roles = self.guild.roles
        msg = ""

        for role in user.roles:
            if role.name.startswith("hide-"):
                msg += f"\n - {role.name[5:]}"
        
        if len(msg) == 0:
            await ctx.send("No channels hidden.")
            return

        embed = self.create_embed(title="Current hide list:", 
                                    text=msg, color=0x0fa3ff,
                                    author=user.name, avatar_url=user.avatar_url)
        await ctx.send(embed=embed)


    @tasks.loop(seconds=3600) 
    async def my_background_task(self):
        await self.check_channels()
        await self.check_roles()

    @my_background_task.before_loop
    async def before_my_task(self):
        await self.bot.wait_until_ready() # wait until the bot logs in

        if not self.guild:
            self.get_guild()


# The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case SimpleCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    bot.add_cog(HideChannel(bot))
