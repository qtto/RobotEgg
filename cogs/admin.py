import datetime
import discord
import os
from discord.ext import commands
from config import config
from cogs.utils.checks import is_owner
from sqlalchemy import create_engine


"""
Some administration commands:::WIP
"""

db   = create_engine(os.environ['DATABASE_URL'])

class Admin(commands.Cog):
    """Several admin commands (doubtlessly useless)"""
    def __init__(self, bot):
        self.bot = bot
        self.startup = datetime.datetime.now()

    def timedelta_str(self, dt):
        days = dt.days
        hours, r = divmod(dt.seconds, 3600)
        minutes, sec = divmod(r, 60)

        if minutes == 1 and sec == 1:
            return f"{days} days, {hours} hours, {minutes} minute and {sec} second."
        elif minutes > 1 and sec == 1:
            return f"{days} days, {hours} hours, {minutes} minutes and {sec} second."
        elif minutes == 1 and sec > 1:
            return f"{days} days, {hours} hours, {minutes} minute and {sec} seconds."
        else:
            return f"{days} days, {hours} hours, {minutes} minutes and {sec} seconds."


    @commands.command()
    @is_owner()
    async def load(self, ctx, extension_name: str):
        """Loads an extension."""
        try:
            self.bot.load_extension(extension_name)
        except (AttributeError, ImportError) as e:
            await ctx.send(f"Error loading extension: {type(e).__name__}, {str(e)}")
            return
        await ctx.send(f"Loaded extension {extension_name}")

    @commands.command()
    @is_owner()
    async def unload(self, ctx, extension_name: str):
        """Unloads an extension."""
        self.bot.unload_extension(extension_name)
        await ctx.send(f"Unloaded {extension_name}.")

    @commands.command()
    @is_owner()
    async def active(self, ctx):
        """lists active extensions."""
        active_ext = ""
        try:
            for cog in tuple(self.bot.cogs):
                active_ext += f"{cog}\n"
        except:
            pass
        await ctx.send(active_ext)

    @commands.command()
    @is_owner()
    async def tables(self, ctx):
        await ctx.send(db.table_names())

    @commands.command()
    @is_owner()
    async def droptable(self, ctx, table_name: str):
        connection = db.raw_connection()
        cursor = connection.cursor()
        command = "DROP TABLE IF EXISTS {};".format(table_name)
        cursor.execute(command)
        connection.commit()
        cursor.close()
        await ctx.send("OK, deleted monkaS")

    @commands.command()
    async def uptime(self, ctx):
        """Prints bot uptime."""
        delta = datetime.datetime.now()-self.startup
        delta_str = self.timedelta_str(delta)
        await ctx.send(f"Uptime: {delta_str}")


    @commands.command()
    @is_owner()
    async def say_this(self, ctx, *args):
        channel = self.bot.get_channel(int(args[0]))
        msg = ' '.join(args[1:])
        await channel.send(msg)


def setup(bot):
    bot.add_cog(Admin(bot))
    