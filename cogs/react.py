import discord
from discord.ext import commands
from config import config


class React(commands.Cog):
    """Reacts to keywords"""

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        args = message.content.lower().split(' ')
        getrole = {'anime': 'BeAdvised', 'mod': 'HyperAdvised', 'egg': 'eggGasm',
                   'dan': 'DanAdvised', 'scringis': 'lionWut'}

        if args[0] == '+getrole' and args[1] in getrole:
            emoji = discord.utils.get(self.bot.emojis, name=getrole[args[1]])
            await message.add_reaction(emoji)

        if 'delet' in args:
            emoji = discord.utils.get(self.bot.emojis, name='nukeplz')
            await message.add_reaction(emoji)

        if '<@&346251636517109770>' in args or '<@&346251636517109770>s' in args:
            emoji = discord.utils.get(self.bot.emojis, name='BeAdvised')
            await message.add_reaction(emoji)

        if 'fuck' in args and 'mathas' in args:
            emoji = discord.utils.get(self.bot.emojis, name='BeAdvised')
            await message.add_reaction(emoji)

        if 'overwatch' in args or 'overwatch,' in args or 'fortnite' in args or 'runescape' in args:
            emoji = discord.utils.get(self.bot.emojis, name='ULU')
            await message.add_reaction(emoji)

        if 'league' in args and args[args.index('league')+1] == 'of' and args[args.index('league')+2] == 'legends':
            emoji = discord.utils.get(self.bot.emojis, name='ULU')
            await message.add_reaction(emoji)

        ricks = ['rick', 'jidril', 'rickon', 'richard', 'dick', 'sheldon', 'young', 'mod', 'bick', 'yeet', 'wek', 'song']
        if ('pickle' in args and args[args.index('pickle')+1] in ricks) or ('siege' in args):
            emoji = discord.utils.get(self.bot.emojis, name='lionSalt')
            await message.add_reaction(emoji)

        if 'delete' in args and args[args.index('delete')+1] == 'this' and args[args.index('delete')+2] == 'server':
            emoji = discord.utils.get(self.bot.emojis, name='nukeplz')
            await message.add_reaction(emoji)

        if 'dead' in args and args[args.index('dead')+1] == 'hours':
            emoji = '💀'
            await message.add_reaction(emoji)

        dontatme = ["dont at me", "don't at me", "dont @ me", "don't @ me"]
        if ' '.join(args[0:3]) in dontatme:
            channel = self.bot.get_channel(346601597335240704)  #bot-commands
            await channel.send(f'<@!{message.author.id}>')

        realeuhours = ["real eu hours", "real eu hour", "false eu hours", "real na hours", "real oce hours"]
        if ' '.join(args[0:3]) in realeuhours:
            emoji = '🇪🇺'
            await message.add_reaction(emoji)

        if args[0].lower() == "suggestion:":
            channel = self.bot.get_channel(346251284455751681)
            if message.channel == channel:
                emojiYea = discord.utils.get(self.bot.emojis, name='voteYea')
                emojiNay = discord.utils.get(self.bot.emojis, name='voteNay')
                await message.add_reaction(emojiYea)
                await message.add_reaction(emojiNay)
#The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case SimpleCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    bot.add_cog(React(bot))

