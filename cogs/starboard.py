import discord
import os
from discord.ext import commands
from config import config
from cogs.utils.checks import channels_allowed
from sqlalchemy import create_engine  
from sqlalchemy import Column, String, Integer, Date  
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base  
from sqlalchemy.orm import sessionmaker

"""
Post multi-reacts in the starboard channel
"""

db   = create_engine(os.environ['DATABASE_URL'])
Base = declarative_base()


class Sb(Base):  
    __tablename__ = "starboard_table"
    msg_id = Column(String, primary_key=True)
    react = Column(String)
    count = Column(Integer)
    pin_id = Column(String)


Session = sessionmaker(db)  
session = Session()
Base.metadata.create_all(db)


class Starboard(commands.Cog):
    """Check reacts"""

    def __init__(self, bot):
        self.bot = bot
        self.starboard = None
        self.mod_commands = None
        self.threshold = 7
        self.denied = []


    async def init_channels(self):
        await self.bot.wait_until_ready()
        self.starboard = self.bot.get_channel(config["channels"]["starboard"])
        self.mod_commands = self.bot.get_channel(config["channels"]["mod-commands"])
        self.posted = {}

    
    def _make_message(self, msg, react=None):
        embed = discord.Embed(color=0x7289da, timestamp=msg.created_at,
                              title=f"in #{msg.channel.name}", description=f"[View message](https://discordapp.com/channels/{msg.guild.id}/{msg.channel.id}/{msg.id})\n\n{msg.content}")
        embed.set_author(name=msg.author.name, icon_url=msg.author.avatar_url)
    
        if react:
            try:
                embed.set_footer(icon_url={react['emoji'].url}, text=f" x{react['count']}")
            except:
                # reaction is not a custom emoji
                embed.set_footer(text=f"{react['emoji']} x{react['count']}")

        if msg.content and len(msg.content) > 4\
            and (msg.content.startswith("http://") or msg.content.startswith("https://"))\
            and msg.content[-4:] in [".jpg", "jpeg", ".png", ".gif"]:
            embed.set_image(url=msg.content)
            
            return embed

        try:
            attch = msg.attachments
            if attch:
                attch_url = attch[0].url
                if attch_url.lower().endswith((
                        'png',
                        'jpeg',
                        'jpg',
                        'gif',
                )):
                    embed.set_image(url=attch_url)
                else:
                    embed.description += '\n'.join([f'[Attachment]({attch_s.url})' for attch_s in attch])
                
                return embed
        except:
            pass
        
        try:
            emb = msg.embeds
            if emb[0] != discord.embeds.Embed.Empty:
                emb_img = emb[0].image
                print(emb_img.url)
                embed.set_image(url=emb_img.url)
                return embed
        except:
            pass
        
        return embed
        
    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        """Fires when someone reacts"""
        msg = reaction.message

        if reaction.count >= self.threshold: 
            if not self.starboard:
                await self.init_channels()

            if msg.id in self.denied:
                return

            if (str(reaction.emoji) != "📌" and str(reaction.emoji) != "⭐") and reaction.count < 10:
                return

            embed = self._make_message(msg, react={"emoji": reaction.emoji, "count": reaction.count})

            item = session.query(Sb).filter_by(msg_id=str(msg.id)).first()
        
            if item:
                if item.react == str(reaction.emoji) and item.count < reaction.count:
                    original_msg = await self.starboard.fetch_message(int(item.pin_id))
                    pin_msg = await original_msg.edit(embed=embed)
                    item.count = reaction.count

            elif await self._confirm_action(embed, msg.id):
                pin_msg = await self.starboard.send(embed=embed)
                pin = Sb(msg_id=str(msg.id), react=str(reaction.emoji), count=reaction.count, pin_id=str(pin_msg.id))
                session.add(pin)
                session.commit()

    
    async def _confirm_action(self, embed, id):
        """Have mod confirm the post"""
        def check(reaction, user):
            """Check if the reaction is by the bot and then if it's an OK or a not OK"""
            if user.id == self.bot.user.id:
                pass
            else:
                return (str(reaction.emoji) == '✅' or str(reaction.emoji) == '🛑')

        if id in self.posted:
            await self.posted[id].delete()
            del self.posted[id]

        msg = await self.mod_commands.send(embed=embed)
        self.posted[id] = msg

        await msg.add_reaction('✅')
        await msg.add_reaction('🛑')

        try:
            react, user = await self.bot.wait_for('reaction_add', timeout=300.0, check=check)
        except:
            react=False

        if react:
            await msg.delete()
            del self.posted[id]

            if str(react.emoji) == '✅':
                return True

            self.denied.append(id)
            return False 
        
        # in case no mod reacts just post it
        await msg.delete()
        del self.posted[id]
        return True


    @commands.command()
    @channels_allowed(["mod-commands"])
    async def star(self, ctx):
        if not self.starboard:
            await self.init_channels() 

        try:
            cmd = ctx.message.content.split(' ')
            message = int(cmd[1])
            if len(cmd)> 2:
                channel = int(cmd[2])
            else:
                channel = config["channels"]["general"]

        except Exception as e:
            await ctx.send(f"Error: {e}. \n\n-star <message_id> <channel_id>")
            return False

        item = session.query(Sb).filter_by(msg_id=str(cmd[1])).first()
        if item:
            await ctx.send(f"Error: this message was already posted in the starboard.")
            return

        try:
            channel = self.bot.get_channel(channel)
            msg = await channel.fetch_message(message)

            embed = self._make_message(msg)

            pin_msg = await self.starboard.send(embed=embed)
            pin = Sb(msg_id=str(msg.id), react="📌", count=5, pin_id=str(pin_msg.id))
            session.add(pin)
            session.commit()

        except Exception as e:
            await ctx.send(f"Error: {e}")

    
    @commands.command()
    @channels_allowed(["mod-commands"])
    async def pins(self, ctx, i):
        try:
            self.threshold = int(i) if (int(i) > 0 and int(i) < 50) else 5
        except:
            await ctx.send("Error: that isn't a proper amount.")
            return
        
        await ctx.send(f"Threshold set to {i}")



# The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case SimpleCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    bot.add_cog(Starboard(bot))
